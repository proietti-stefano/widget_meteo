document.addEventListener('DOMContentLoaded', function () {

    var weatherLondonAPI = 'https://api.openweathermap.org/data/2.5/weather?q=London&appid=3a9ec44cb0eb2955a73233792ea2e355'

    fetch(weatherLondonAPI).then(resp => resp.json()).then(data => {
        console.log(data);

        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

        var date = new Date();

        document.querySelector('.display_box').innerHTML =
            `
               <h1>${data.name} </h1>
               <div class="wtr_details">
                   <div class="day_temp">
                       <p><b>${days[date.getDay()]}</b></p>
                       <p>${Math.round(data.main.temp - 273, 15)}°</p>
                   </div>
                   <div class="wtr_box">
                       <img src="${'https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/' + data.weather[0].icon + '.svg'}">
                       <p id="wtr_description">${data.weather[0].description}</p>
                   </div>
               </div>
           `
    });


    document.getElementById('btn_src').addEventListener('click', searchCity);

    function searchCity() {

        let cityName = document.getElementById('city_src').value;

        let meteoAPIbyCity = 'https://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&appid=3a9ec44cb0eb2955a73233792ea2e355';

        fetch(meteoAPIbyCity).then(resp => resp.json()).then(data => {

            console.log(data);

            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

            var date = new Date();

            document.querySelector('.display_box').innerHTML = '';

            document.querySelector('.display_box').innerHTML =
                `
               <h1>${data.name} </h1>
               <div class="wtr_details">
                   <div class="day_temp">
                       <p><b>${days[date.getDay()]}</b></p>
                       <p>${Math.round(data.main.temp - 273, 15)}°</p>
                   </div>
                   <div class="wtr_box">
                       <img src="${'https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/' + data.weather[0].icon + '.svg'}">
                       <p id="wtr_description">${data.weather[0].description}</p>
                   </div>
               </div>
           `

            document.getElementById('city_src').value = '';

        })

    }





})
